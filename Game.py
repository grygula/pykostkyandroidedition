
from pygame.locals import *
from vec2 import *
from PyKostky import FPS
from PyKostky import PPM
from PyKostky import SCREEN_WIDTH
from PyKostky import SCREEN_HEIGHT
from PyKostky import TIME_STEP
from PyKostky import StartX
from PyKostky import StartY
from Box2D import world



from Ground import *
from Box import *

from Ball import *
from ContactListener import *

class Game(object):
    def __init__(self, s,initPosBoxes,initPosPad,platforms):
        self.isGameLoop = 0
        self.screen = s
        self.remList = []
        self.objects = []
        self.boxList = []
        self.createBox2DWorld()
        self.setGameObjects(initPosBoxes,initPosPad,platforms)
        self.clock = pygame.time.Clock()
        self.ballsLimit = 2
        self.shouldRefresh=1
    def loop(self):
        self.isGameLoop = 1
        while self.isGameLoop == 1:
            for event in pygame.event.get():
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    self.isGameLoop = 0
                if event.type == pygame.MOUSEBUTTONDOWN or event.type == pygame.MOUSEBUTTONUP or event.type == pygame.MOUSEMOTION:
                    #self.acCir.onMouseEvent(event)
                    self.ball.onMouseEvent(event)
                    pass
            
            self.draw()

        self.exitGame()

    def exitGame(self):
        self.isGameLoop = 0
        pygame.quit()
        exit()

    def draw(self):
        self.screen.fill((0, 0, 0, 255))

        for obj in self.objects:
            obj.update(TIME_STEP)
            obj.draw()
        self.world.step(TIME_STEP, 10, 10)
        self.world.clear_forces()

        for obj in self.remList:
            print obj
            obj.remove()
            self.objects.remove(obj)
            self.remList.remove(obj)
            self.boxList.remove(obj)
        
        temp = self.updateAndCheckWinLooseConditions()
        
        if(self.shouldRefresh==1):
            pygame.display.flip()
            self.shouldRefresh=temp

        self.clock.tick(FPS)
    
    def createBox2DWorld(self):
        self.gravity = (0, -10)
        self.world = World(gravity = self.gravity, do_sleep = True)
        self.world.contactListener = ContactListener()
        self.world.contactListener.setGame(self)
    
    def updateAndCheckWinLooseConditions(self):
        ballsLeft = self.ballsLimit - self.ball.resetCount;
        boxLeft = len( self.boxList);
        
        myFont = pygame.font.Font('freesansbold.ttf', 15)
        ballsLeftTxt = myFont.render('Balls left: '+str(ballsLeft), True, (255, 255, 255))
        boxLeftTxt = myFont.render('Box left: ' +str(boxLeft), True, (255, 255, 255))
        
        self.screen.blit(ballsLeftTxt, ((4), (10)))
        self.screen.blit(boxLeftTxt, ((self.screen.get_width()-90), (10)))
        
        if(ballsLeft<0):
            self.lose()
            return 0

        if(boxLeft<0):
            self.won()
            return 0 
        
        return 1;
    def won(self):
        titleFont = pygame.font.Font('freesansbold.ttf', 80)
        gm = titleFont.render('You WON', True, (255, 255, 255))
        self.screen.blit(gm, ((self.screen.get_width()/2-200), (150)))

        
    def lose(self):
        titleFont = pygame.font.Font('freesansbold.ttf', 80)
        gm = titleFont.render('You Lose', True, (255, 255, 255))
        self.screen.blit(gm, ((self.screen.get_width()/2-200), (150)))                             

    def remove(self, box):
        self.remList.append(box)

    def setGameObjects(self,initPosBoxes,initPosPad,platforms):
        self.ground = Ground(self.screen ,self.world)
        self.ground.createBody((40, 0.1))
        self.ground.setColor((255, 0, 0, 255))
        self.ground.setUserBody("floor")
        self.objects.append(self.ground)

        self.upGround = Ground(self.screen, self.world)
        self.upGround.createBody((40, 0.1))
        self.upGround.setPosition((0, 24))
        self.upGround.setColor((255, 0, 0, 255))
        self.objects.append(self.upGround)

        self.leftGround = Ground(self.screen, self.world)
        self.leftGround.createBody((0.1, 24))
        self.leftGround.setPosition((0, 0))
        self.leftGround.setColor((255, 0, 0, 255))
        self.objects.append(self.leftGround)

        self.rightGround = Ground(self.screen, self.world)
        self.rightGround.createBody((0.1, 24))
        self.rightGround.setPosition((40, 0))
        self.rightGround.setColor((255, 0, 0, 255))
        self.objects.append(self.rightGround)
    
        for platformPos in platforms:
            platform = Ground(self.screen, self.world)
            platform.createBody((1, 0.5))
            platform.setPosition(platformPos)
            platform.setColor((0, 255, 0, 255))
            platform.setUserBody = "platform"
            self.objects.append(platform)
        for initBoxPos in initPosBoxes:
            box = Box(self.screen, self.world)
            box.createBody((1, 1))
            box.setPosition(initBoxPos)
            box.setColor((255, 255, 0, 255))
            self.objects.append(box)
            self.boxList.append(box)
        self.ball = Ball(self.screen, self.world,initPosPad)
        self.objects.append(self.ball);
