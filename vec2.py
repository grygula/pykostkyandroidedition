from math import *

class vec2 :
    def __init__(self, x, y) :
        self.x = x
        self.y = y

    def set(self, vec) :
        self.x = vec.x
        self.y = vec.y

    def get_x(self) :
        return self.x

    def get_y(self):
        return self.y

    def add(self, vec) :
        self.x += vec.x
        self.y += vec.y

    def scale(self, scalar) :
        self.x *= scalar
        self.y *= scalar

    def get_magnitude(self) :
        return sqrt((self.x * self.x) + (self.y * self.y))

    def get_normalized(self) :
        magnitude = self.get_magnitude()
        if(magnitude == 0.0) :
            return Vec2(0.0, 0.0)
        return Vec2(self.x / magnitude, self.y / magnitude)
