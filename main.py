import pygame, sys, random, time,menus,json
from pygame import draw, display, rect
from pygame.locals import *
from Game import Game

try:
    import android
except ImportError:
    android = None

def getBoardData(dom,selected):
    boxsc=[]
    platforms=[]
    board = dom[selected]
    pad = board["pad"]
    padCords=(int(pad["x"]),int(pad["y"]))
    bxs = board["bxs"]
    for bx in bxs:
        boxsc.append((int(bx["x"]),int(bx["y"])))
    ptfms = board["platforms"]
    for ptfm in ptfms:
        platforms.append((int(ptfm["x"]),int(ptfm["y"])))
    return [boxsc,padCords,platforms]

def renderBoardsOptions(s):
    json_data = open('data.json')
    data = json.load(json_data)
    xmlbrds=data["boards"]
    brds=[]
    for xmlb in xmlbrds:
        brds.append(xmlb["name"])
    selectedboard=menus.showBoardsOptions(s,brds)
    d=getBoardData(xmlbrds,selectedboard)
    json_data.close()
    game = Game(s,d[0],d[1],d[2])
    game.loop()

def showMenus(s,c):
    if c==1:
        p = menus.showMenu(s)
        showMenus(s,p)
        return
    elif c==3:
        renderBoardsOptions(s);
        return
    elif c==2:
        p= menus.showAbout(s)
        showMenus(s,p)
        return
    else:
        p = menus.showMenu(s)
        showMenus(s,p)
        return
    
def main():
    pygame.init()
    if android:
        android.init()
        android.map_key(android.KEYCODE_BACK, pygame.K_ESCAPE)
    MAINCLOCK = pygame.time.Clock()
    s = pygame.display.set_mode((800, 480))
    showMenus(s,1)

if __name__ == '__main__':
    main()

