import pygame
from pygame import draw, display, rect
from pygame.locals import *
WHITE     = (255, 255, 255)
BLACK     = (  0,   0,   0)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
DARKGREEN = (  0, 155,   0)
DARKGRAY  = ( 40,  40,  40)
BGCOLOR = BLACK
def showMenu(s):
    titleFont = pygame.font.Font('freesansbold.ttf', 80)
    gm = titleFont.render('Game', True, WHITE, DARKGREEN)
    ab = titleFont.render('About', True, GREEN)
    while True:
        s.fill(BGCOLOR)
        gmb = s.blit(gm, ((s.get_width()/2-50), (100)))
        abb = s.blit(ab, ((s.get_width()/2-50), (300)))
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                return 0
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if gmb.collidepoint(pos):
                    return 3
                if abb.collidepoint(pos):
                    return 2
        pygame.display.update()
def showAbout(s):
    titleFont = pygame.font.Font('freesansbold.ttf', 80)
    textFont = pygame.font.Font('freesansbold.ttf', 40)
    gm = textFont.render('Game created for UJ', True, WHITE, DARKGREEN)
    ab = titleFont.render('Back', True, GREEN)
    while True:
        s.fill(BGCOLOR)
        gmb = s.blit(gm, ((s.get_width()/2-100), (100)))
        abb = s.blit(ab, ((s.get_width()/2-50), (300)))
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                return 0
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if abb.collidepoint(pos):
                    return 1
        pygame.display.update()

def showBoardsOptions(s,xmlbrds):
    font = pygame.font.Font('freesansbold.ttf', 20)
    brds=[]
    for xmlb in xmlbrds:
        tf = font.render(xmlb, True, GREEN)
        brds.append(tf)
    while True:
        s.fill(BGCOLOR)
        brdsbxs=[]
        i=0
        for brd in brds:
            i=i+1
            tb=s.blit(brd, ((s.get_width()/2-150), (i*80)))
            brdsbxs.append(tb)
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                return 0
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                z=0
                for bx in brdsbxs:
                    if bx.collidepoint(pos):
                        return z
                    z=z+1
        pygame.display.update()
